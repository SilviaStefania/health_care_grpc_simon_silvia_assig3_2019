package com.example.pillBox.grpc;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.15.0)",
    comments = "Source: getMed.proto")
public final class medicationPlansGrpc {

  private medicationPlansGrpc() {}

  public static final String SERVICE_NAME = "medicationPlans";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.example.pillBox.grpc.GetMed.MedRequest,
      com.example.pillBox.grpc.GetMed.MedResponse> getMedMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "med",
      requestType = com.example.pillBox.grpc.GetMed.MedRequest.class,
      responseType = com.example.pillBox.grpc.GetMed.MedResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.example.pillBox.grpc.GetMed.MedRequest,
      com.example.pillBox.grpc.GetMed.MedResponse> getMedMethod() {
    io.grpc.MethodDescriptor<com.example.pillBox.grpc.GetMed.MedRequest, com.example.pillBox.grpc.GetMed.MedResponse> getMedMethod;
    if ((getMedMethod = medicationPlansGrpc.getMedMethod) == null) {
      synchronized (medicationPlansGrpc.class) {
        if ((getMedMethod = medicationPlansGrpc.getMedMethod) == null) {
          medicationPlansGrpc.getMedMethod = getMedMethod = 
              io.grpc.MethodDescriptor.<com.example.pillBox.grpc.GetMed.MedRequest, com.example.pillBox.grpc.GetMed.MedResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "medicationPlans", "med"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.example.pillBox.grpc.GetMed.MedRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.example.pillBox.grpc.GetMed.MedResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new medicationPlansMethodDescriptorSupplier("med"))
                  .build();
          }
        }
     }
     return getMedMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.example.pillBox.grpc.GetMed.MedOkRequest,
      com.example.pillBox.grpc.GetMed.MedOkResponse> getMedOkMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "medOk",
      requestType = com.example.pillBox.grpc.GetMed.MedOkRequest.class,
      responseType = com.example.pillBox.grpc.GetMed.MedOkResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.example.pillBox.grpc.GetMed.MedOkRequest,
      com.example.pillBox.grpc.GetMed.MedOkResponse> getMedOkMethod() {
    io.grpc.MethodDescriptor<com.example.pillBox.grpc.GetMed.MedOkRequest, com.example.pillBox.grpc.GetMed.MedOkResponse> getMedOkMethod;
    if ((getMedOkMethod = medicationPlansGrpc.getMedOkMethod) == null) {
      synchronized (medicationPlansGrpc.class) {
        if ((getMedOkMethod = medicationPlansGrpc.getMedOkMethod) == null) {
          medicationPlansGrpc.getMedOkMethod = getMedOkMethod = 
              io.grpc.MethodDescriptor.<com.example.pillBox.grpc.GetMed.MedOkRequest, com.example.pillBox.grpc.GetMed.MedOkResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "medicationPlans", "medOk"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.example.pillBox.grpc.GetMed.MedOkRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.example.pillBox.grpc.GetMed.MedOkResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new medicationPlansMethodDescriptorSupplier("medOk"))
                  .build();
          }
        }
     }
     return getMedOkMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static medicationPlansStub newStub(io.grpc.Channel channel) {
    return new medicationPlansStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static medicationPlansBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new medicationPlansBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static medicationPlansFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new medicationPlansFutureStub(channel);
  }

  /**
   */
  public static abstract class medicationPlansImplBase implements io.grpc.BindableService {

    /**
     * <pre>
     *numele dupa rpc tre sa fie la fel ca in client
     *in primele () input parameters
     * </pre>
     */
    public void med(com.example.pillBox.grpc.GetMed.MedRequest request,
        io.grpc.stub.StreamObserver<com.example.pillBox.grpc.GetMed.MedResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getMedMethod(), responseObserver);
    }

    /**
     */
    public void medOk(com.example.pillBox.grpc.GetMed.MedOkRequest request,
        io.grpc.stub.StreamObserver<com.example.pillBox.grpc.GetMed.MedOkResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getMedOkMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getMedMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.example.pillBox.grpc.GetMed.MedRequest,
                com.example.pillBox.grpc.GetMed.MedResponse>(
                  this, METHODID_MED)))
          .addMethod(
            getMedOkMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.example.pillBox.grpc.GetMed.MedOkRequest,
                com.example.pillBox.grpc.GetMed.MedOkResponse>(
                  this, METHODID_MED_OK)))
          .build();
    }
  }

  /**
   */
  public static final class medicationPlansStub extends io.grpc.stub.AbstractStub<medicationPlansStub> {
    private medicationPlansStub(io.grpc.Channel channel) {
      super(channel);
    }

    private medicationPlansStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected medicationPlansStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new medicationPlansStub(channel, callOptions);
    }

    /**
     * <pre>
     *numele dupa rpc tre sa fie la fel ca in client
     *in primele () input parameters
     * </pre>
     */
    public void med(com.example.pillBox.grpc.GetMed.MedRequest request,
        io.grpc.stub.StreamObserver<com.example.pillBox.grpc.GetMed.MedResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getMedMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void medOk(com.example.pillBox.grpc.GetMed.MedOkRequest request,
        io.grpc.stub.StreamObserver<com.example.pillBox.grpc.GetMed.MedOkResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getMedOkMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class medicationPlansBlockingStub extends io.grpc.stub.AbstractStub<medicationPlansBlockingStub> {
    private medicationPlansBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private medicationPlansBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected medicationPlansBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new medicationPlansBlockingStub(channel, callOptions);
    }

    /**
     * <pre>
     *numele dupa rpc tre sa fie la fel ca in client
     *in primele () input parameters
     * </pre>
     */
    public com.example.pillBox.grpc.GetMed.MedResponse med(com.example.pillBox.grpc.GetMed.MedRequest request) {
      return blockingUnaryCall(
          getChannel(), getMedMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.example.pillBox.grpc.GetMed.MedOkResponse medOk(com.example.pillBox.grpc.GetMed.MedOkRequest request) {
      return blockingUnaryCall(
          getChannel(), getMedOkMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class medicationPlansFutureStub extends io.grpc.stub.AbstractStub<medicationPlansFutureStub> {
    private medicationPlansFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private medicationPlansFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected medicationPlansFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new medicationPlansFutureStub(channel, callOptions);
    }

    /**
     * <pre>
     *numele dupa rpc tre sa fie la fel ca in client
     *in primele () input parameters
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<com.example.pillBox.grpc.GetMed.MedResponse> med(
        com.example.pillBox.grpc.GetMed.MedRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getMedMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.example.pillBox.grpc.GetMed.MedOkResponse> medOk(
        com.example.pillBox.grpc.GetMed.MedOkRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getMedOkMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_MED = 0;
  private static final int METHODID_MED_OK = 1;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final medicationPlansImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(medicationPlansImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_MED:
          serviceImpl.med((com.example.pillBox.grpc.GetMed.MedRequest) request,
              (io.grpc.stub.StreamObserver<com.example.pillBox.grpc.GetMed.MedResponse>) responseObserver);
          break;
        case METHODID_MED_OK:
          serviceImpl.medOk((com.example.pillBox.grpc.GetMed.MedOkRequest) request,
              (io.grpc.stub.StreamObserver<com.example.pillBox.grpc.GetMed.MedOkResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class medicationPlansBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    medicationPlansBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.example.pillBox.grpc.GetMed.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("medicationPlans");
    }
  }

  private static final class medicationPlansFileDescriptorSupplier
      extends medicationPlansBaseDescriptorSupplier {
    medicationPlansFileDescriptorSupplier() {}
  }

  private static final class medicationPlansMethodDescriptorSupplier
      extends medicationPlansBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    medicationPlansMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (medicationPlansGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new medicationPlansFileDescriptorSupplier())
              .addMethod(getMedMethod())
              .addMethod(getMedOkMethod())
              .build();
        }
      }
    }
    return result;
  }
}
