package com.example.pillBox;

import com.example.pillBox.grpc.GetMed;
import com.example.pillBox.grpc.medicationPlansGrpc;
import lombok.Data;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static javax.swing.BorderFactory.createEmptyBorder;

@Data
public class MainFrame extends JFrame {

    private String[] args;
    private String allMeds;
    private String medOk = "no";
    private String currentMedName ;
    private  medicationPlansGrpc.medicationPlansBlockingStub medStub;

    private int guard = 0;

    Date date = new Date();
    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");


    public MainFrame(String title, String allMeds,  medicationPlansGrpc.medicationPlansBlockingStub medStub ) {
        super();
        Map<JButton, JLabel> buttonJLabelMap = new HashMap<>();
        setTitle(title);
        this.medStub = medStub;
        this.allMeds = allMeds;
        setSize(640, 480);
        init();
        try {
            buttonJLabelMap = initComponents(buttonJLabelMap);
            while (true) {
                checkMedication(buttonJLabelMap);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }

    private void checkMedication(Map<JButton, JLabel> map) {
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 0;
        map.keySet().forEach(button -> {
            String label = map.get(button).getText();
            String[] data = label.split(",");
            String[] timeIntervals = data[1].split(" ");
            String[] hourStart = timeIntervals[0].split(":");
            String[] hourEnd = timeIntervals[1].split(":");

            String start = hourStart[0];
            String end = hourEnd[0];

            if(Integer.parseInt(start) < LocalDateTime.now().getSecond() && Integer.parseInt(end) > LocalDateTime.now().getSecond()) {
                add(map.get(button), constraints);
                constraints.gridy += 1;
                button.setEnabled(true);
                add(button, constraints);
                constraints.gridy += 1;
            }
            else {
                button.setEnabled(false);
                if(guard == 0){
                    GetMed.MedOkRequest medOkRequest = GetMed.MedOkRequest.newBuilder().setMedOk(medOk).setMedOkName(currentMedName).build();
                    medStub.medOk(medOkRequest);
                    guard = 1;
                }


            }
        });
    }

    public void setArgs(String[] args) {
        this.args = args;
    }

    public void init() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // pack();
        setLayout(new GridLayout(5,5));
        setVisible(true);


    }

    private Map<JButton, JLabel> initComponents(Map<JButton, JLabel> map) throws InterruptedException {
        GridBagConstraints constraints = new GridBagConstraints();
        JButton formsBtn = new JButton("Button");

        constraints.gridx = 0;
        constraints.gridy = 0;
        JLabel labelTime = new JLabel(formatter.format(date));
        this.add(labelTime, constraints);
        String[] allMedsSplit =  allMeds.split(";");
        constraints.gridx = 1;
        constraints.gridy = 1;


        for (int i = 0; i < allMedsSplit.length-1 ; i++) {
            String[] individualMed =  allMedsSplit[i].split("\\s+");
            String medName = individualMed[0];
            currentMedName = medName;
            String start = individualMed[1];
            String startH = start.split(":")[0];
            String stop = individualMed[2];
            String stopH = stop.split(":")[0];
            String stopM = stop.split(":")[1];
            final String wrapper =medName;

            String timeText = labelTime.getText().toString().split("[: ]+")[1];
            //daca current time nu ii in interval pastila -> nu afisez pastila
        //    if(Integer.parseInt(startH) < Integer.parseInt(timeText) && Integer.parseInt(stopH) >= Integer.parseInt(timeText)){
             // if(Integer.parseInt(startH) < LocalDateTime.now().getSecond() && Integer.parseInt(stopH) >= LocalDateTime.now().getSecond()){


                JLabel label1 = new JLabel(medName + "," + start + " " + stop);

                /*add(label1, constraints);
                constraints.gridy += 1;*/

                JButton taken = new JButton("I took the pill");
                /*add(taken, constraints);
                constraints.gridy += 1;*/

                map.put(taken, label1);

                taken.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        label1.setText("Registered");
                        medOk = "yes";
                        GetMed.MedOkRequest medOkRequest = GetMed.MedOkRequest.newBuilder().setMedOk(medOk).setMedOkName(wrapper).build();
                        medStub.medOk(medOkRequest);
                        taken.setEnabled(false);
                    }
                });

                //aici LocalDateTime.now().getMinute e cum ar fi ora, am fascut asa pentru test
                //adica daca a expirat intervalul
              //  Integer localTime = LocalDateTime.now().getMinute();
                Integer localTime = LocalDateTime.now().getSecond();
                if (localTime >= Integer.parseInt(stopH)) {
                    GetMed.MedOkRequest medOkRequest = GetMed.MedOkRequest.newBuilder().setMedOk(medOk).setMedOkName(medName).build();
                    medStub.medOk(medOkRequest);
                    //taken.setEnabled(false);
                }
         /*   }else{
                GetMed.MedOkRequest medOkRequest = GetMed.MedOkRequest.newBuilder().setMedOk(medOk).setMedOkName(medName).build();
               medStub.medOk(medOkRequest);
           }*/
        }
        Timer SimpleTimer = new Timer(1000, new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                labelTime.setText(formatter.format(new Date()));
            }
        });
        SimpleTimer.start();


        return map;


    }


}