package com.example.pillBox;

import com.example.pillBox.grpc.GetMed;
import com.example.pillBox.grpc.medicationPlansGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

import lombok.Data;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

@Data
@SpringBootApplication
public class PillBoxApplication  {

    public static void main(String[] args) {

        SpringApplicationBuilder builder = new SpringApplicationBuilder(PillBoxApplication.class);
        builder.headless(false);
        ConfigurableApplicationContext context = builder.run(args);

        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 9091).usePlaintext().build();

        //we need to call stubs from generated protofile deci am nevoie si aici de chestiile genrate in server
        medicationPlansGrpc.medicationPlansBlockingStub medStub = medicationPlansGrpc.newBlockingStub(channel);

        GetMed.MedRequest medRequest = GetMed.MedRequest.newBuilder().setPatientId("3").setMedPlanRank("3").build();
        GetMed.MedResponse response = medStub.med(medRequest);

        String allMeds = response.getMedNameResponse();
        System.out.println(allMeds );


        MainFrame mainFrame = new MainFrame("String", allMeds, medStub);
        String statusMedOk = mainFrame.getMedOk();
        String medNameOk = mainFrame.getCurrentMedName();

      //  GetMed.MedOkRequest medOkRequest = GetMed.MedOkRequest.newBuilder().setMedOk(statusMedOk).setMedOkName(medNameOk).build();


    }




}