package com.example.springdemo.repositories;

import com.example.springdemo.entities.Activity;
import com.example.springdemo.entities.AppUser;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.Optional;

public interface ActivityRepository extends Repository<Activity, Integer> {

    Activity save(Activity activity);
    List<Activity> findAll();
    Optional<Activity> findById(int id);
    void delete(Activity activity);

}

