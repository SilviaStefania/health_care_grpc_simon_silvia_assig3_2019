package com.example.springdemo.repositories;


import org.springframework.stereotype.Component;


public interface RepositoryFactory {

    AppUserRepository createAllUsersRepository();
    MedicationRepository createMedicationRepository();
    MedicalPlanRepository createMedicalPlanRepository();
    ActivityRepository createActivityRepository();


}
