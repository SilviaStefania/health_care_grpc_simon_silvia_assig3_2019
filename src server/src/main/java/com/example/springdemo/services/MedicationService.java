package com.example.springdemo.services;

import com.example.springdemo.entities.Medication;
import com.example.springdemo.repositories.RepositoryFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class MedicationService {

    private final RepositoryFactory repositoryFactory;

    public Medication save(Medication medication){
        return repositoryFactory.createMedicationRepository().save(medication);
    }

    public void removeMedication(Medication medication){
        repositoryFactory.createMedicationRepository().delete(medication);
    }

    public Optional<Medication> findById(int id){
        return repositoryFactory.createMedicationRepository().findById(id);
    }

    public List<Medication> findAllMedication(){
        return repositoryFactory.createMedicationRepository().findAll();
    }



}
