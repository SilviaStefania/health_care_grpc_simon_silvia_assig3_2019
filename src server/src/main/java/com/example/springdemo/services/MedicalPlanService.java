package com.example.springdemo.services;

import com.example.springdemo.entities.AppUser;
import com.example.springdemo.entities.MedicalPlan;
import com.example.springdemo.entities.Medication;
import com.example.springdemo.repositories.RepositoryFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class MedicalPlanService {

    private final RepositoryFactory repositoryFactory;

    public MedicalPlan save(MedicalPlan medicalPlan) {
        return repositoryFactory.createMedicalPlanRepository().save(medicalPlan);
    }

    public void removeMedicalPlan(MedicalPlan medicalPlan) {
        repositoryFactory.createMedicalPlanRepository().delete(medicalPlan);
    }

    public Optional<MedicalPlan> findById(int id) {
        return repositoryFactory.createMedicalPlanRepository().findById(id);
    }

    public List<MedicalPlan> findAllMedicalPlan() {
        return repositoryFactory.createMedicalPlanRepository().findAll();
    }


    public List<MedicalPlan> findAllMedicalPlansForPatient(AppUser appUser){
        return repositoryFactory.createMedicalPlanRepository().findMedicalPlansByPatient(appUser);
    }




}
