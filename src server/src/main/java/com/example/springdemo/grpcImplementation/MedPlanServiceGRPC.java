package com.example.springdemo.grpcImplementation;

import com.example.springdemo.dto.MedicalPlanDTO;
import com.example.springdemo.entities.MedicalPlan;
import com.example.springdemo.entities.Medication;
import com.example.springdemo.grpc.GiveMed;
import com.example.springdemo.grpc.medicationPlansGrpc;
import com.example.springdemo.entities.AppUser;
import com.example.springdemo.repositories.RepositoryFactory;
import com.example.springdemo.services.MedicalPlanService;
import io.grpc.stub.StreamObserver;
import lombok.RequiredArgsConstructor;
import org.apache.catalina.LifecycleState;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor

public class MedPlanServiceGRPC extends medicationPlansGrpc.medicationPlansImplBase {
    private final RepositoryFactory repositoryFactory;

    @Override
    public void medOk(GiveMed.MedOkRequest request, StreamObserver<GiveMed.MedOkResponse> responseObserver) {
        GiveMed.MedOkResponse.Builder response = GiveMed.MedOkResponse.newBuilder();
        System.out.println("Med taken?");
        String medStatus = request.getMedOk();
        String medNameStatus = request.getMedOkName();
        System.out.println(medNameStatus + " " +medStatus);
        //send data back
        responseObserver.onNext(response.build());

        //close the call
        responseObserver.onCompleted();
    }

    @Override
    public void med(GiveMed.MedRequest request ,StreamObserver<GiveMed.MedResponse> responseObserver) {
        System.out.println("Inside med");
        String requestedPatientId = request.getPatientId();
        String requestedMedPlanId = request.getMedPlanRank();

        System.out.println(requestedPatientId);

        GiveMed.MedResponse.Builder response = GiveMed.MedResponse.newBuilder();

        if (repositoryFactory.createAllUsersRepository().findById(Integer.parseInt(requestedPatientId)).isPresent()) {
            AppUser patient = repositoryFactory.createAllUsersRepository().findById(Integer.parseInt(requestedPatientId)).get();

            List<MedicalPlan> medPlansForPatient = repositoryFactory.createMedicalPlanRepository().findMedicalPlansByPatient(patient);
            List<MedicalPlanDTO> medicalPlanDTOS = new ArrayList<>();

            MedicalPlan currentPlan = medPlansForPatient.get(Integer.parseInt(requestedMedPlanId));

            List<MedicalPlan> currentPlanWrapper = new ArrayList<>();
            currentPlanWrapper.add(currentPlan);
            List<String> medicationsName = repositoryFactory.createMedicationRepository().findAllByMedicalPlans(currentPlanWrapper).stream().
                    map(med -> med.getMedName()).collect(Collectors.toList());
            medicalPlanDTOS.add(new MedicalPlanDTO(medicationsName, currentPlan));

            if (medPlansForPatient.isEmpty()) {
                response.setMedNameResponse("Patient does not have med plans");
            } else {
                response.setMedNameResponse(medicalPlanDTOS.toString());
            }
        } else {
            response.setMedNameResponse("No such Patient");
        }


        //send data back
        responseObserver.onNext(response.build());

        //close the call
        responseObserver.onCompleted();



    }
}
