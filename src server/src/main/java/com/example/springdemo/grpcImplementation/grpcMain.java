package com.example.springdemo.grpcImplementation;

import com.example.springdemo.repositories.RepositoryFactory;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class grpcMain implements CommandLineRunner {

    private final RepositoryFactory repositoryFactory;

    @Override
    public void run(String... args) throws Exception {
        Server server = ServerBuilder.forPort(9091).addService(new MedPlanServiceGRPC(repositoryFactory)).build();
        server.start();
        System.out.println("server started at " + server.getPort() );

        server.awaitTermination();

    }
}
