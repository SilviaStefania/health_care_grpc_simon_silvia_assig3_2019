package com.example.springdemo.dto;

import com.example.springdemo.entities.MedicalPlan;
import com.example.springdemo.entities.Medication;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MedicalPlanDTO {

    private List<String> medsName;
    private String[] medIntakeInterval;

    public MedicalPlanDTO(MedicalPlan medicalPlan){
        medicalPlan.getMedications().stream().forEach(e-> medsName.add(e.getMedName()));
        this.medIntakeInterval = medicalPlan.getTimePeriod().split(",");
    }

    public  MedicalPlanDTO(List<String> medsName, MedicalPlan medicalPlan){
        this.medsName = medsName;
        this.medIntakeInterval = medicalPlan.getTimePeriod().split(",");
    }


    public String toString(){
         String finalString ="" ;
        for (int i = 0; i < medsName.size(); i++) {
            finalString+=medsName.get(i) + " " + medIntakeInterval[i] + ";";
        }
        return finalString;
    }

}
