package com.example.springdemo.dto;


import com.example.springdemo.entities.MedicalPlan;
import com.example.springdemo.entities.Medication;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MedicalPlansSeenByPatient {

    private String doctorName;
    private String startMed;
    private String stopMed;
    private String timePeriod;
    private String meds = "";

    public MedicalPlansSeenByPatient(MedicalPlan medicalPlan){
        this.doctorName = medicalPlan.getDoctor().getName();
        this.startMed = medicalPlan.getStartMed();
        this.stopMed = medicalPlan.getStopMed();
        this.timePeriod = medicalPlan.getTimePeriod();
        for(Medication m: medicalPlan.getMedications()){
            this.meds += m.toString();
        }
    }


}
