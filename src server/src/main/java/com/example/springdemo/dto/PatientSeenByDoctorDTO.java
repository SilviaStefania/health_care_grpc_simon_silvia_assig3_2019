package com.example.springdemo.dto;

import com.example.springdemo.entities.AppUser;
import com.example.springdemo.entities.MedicalPlan;
import com.fasterxml.jackson.databind.deser.std.MapEntryDeserializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PatientSeenByDoctorDTO {

    private Integer id;
    private String name;
    private String address;
    private String gender;
    private String bday;
  //  List<MedicalPlan> medicalPlans;

    public PatientSeenByDoctorDTO(AppUser appUser){
        this.id = appUser.getAppUserId();
        this.name = appUser.getName();
        this.address = appUser.getAddress();
        this.gender = appUser.getGender();
        this.bday = appUser.getBday();
    }


}
